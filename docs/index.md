# Vorlesungen aus dem AudiMax

Bei Videoaufnahmen aus dem AudiMax werden die zwei Dateien `screen.avi` und `camera.avi`.

Wenn Vorlesungen im AudiMax aufgenommen wurden, werden zwei Dateien bereitgestellt:

- Die Datei `screen.avi` ist die Videoaufnahmen der Präsentation.
- Die Datei `camera.avi` ist die Videoaufnahme des Dozenten.

Die Farben der Videodatei `screen.avi` sind nicht originalgetreu, deswegen muss die Videoaufnahme mit Bildern von den Folien überlagert werden.

# Vorlesungen mit Camtasia Aufzeichnungen

# Quick Tutorials

## Start festlegen
![](img/SetStart.gif)

## Ende festlegen


## Folien einfügen
![](img/InsertSlides.gif)

Erhöhe die Zoomstufe (drücke drei Mal auf das `+` Zeichen).

Füge die Folie auf die entsprechende Spur ein.

Durchsuche mit Hilfe des Schiebereglers das Video bis das Bild auf die nächste Folie springt.

Um die genaue Position des Folienübergangs zu bestimmen, können die 
Benutze die Tasten <kbd>,</kbd> (Ein Frame zurück) und <kbd>.</kbd> (Ein Frame vor), um den Schieberegler genau an die Stelle zusetzen, an der die nächste Folie angezeigt wird.

Verlängere mit gedrückter linker Maustaste die Anzeige der Folie bis zum Schieberegler.

Drücke die Tastenkombination <kbd>Shift</kbd> + <kbd>M</kbd> , um eine Markierung zuerstellen.

Klicke auf das Textfeld oben rechts, um der Markierung einen anderen Namen zu geben.

## Hintergrundfolien einfügen


## Tonspuren synchronisieren
![](img/AudioSynchronization.gif)
Um zwei oder mehrere Tonspuren zu synchronisieren, müssen die Ausschläge aller Tonspuren übereinander liegen.
Benutze die verschiedenen Zoomstufen, da auf höchster Zoomstufe es oft schwierig ist Ähnlichkeiten zu erkennen.
Falls es schwierig ist die Ausschläge der Tonaufnahmen optisch zueinander zuzuordnen, müssen die Tonaufnahmen auditiv miteinander verglichen werden.
Hierbei ist es am einfachsten den Start des Vortrags zu identifizieren.
Verschiebe anschließend die Tonspuren mit Hilfe der Maus, so dass die Ausschläge beider Tonspuren übereinander liegen.

## Seitenverhältnis einer Aufnahme korrigieren
![](img/CorrectAspectRatio.gif)

Es kann vorkommen, dass eine Aufnahme im falschen Seitenverhätnis vorliegt.
Beispielsweise hat die Datei `screen.avi` ein 16:9 Seitenverhältnis.
Die Aufnahme muss in ein 4:3 Seitenverhältnis gebracht werden.
Hierzu setzt man eine 4:3 Auflösung für Camtasia Leinwand fest.
Anschließend passt man das Video der zuvor eingestellten Auflösung an.
Standardmäßig skaliert Camtasia die Videos immer mit dem erkannten Seitenverhätlnis.
Durch Drücken der <kbd>SHIFT</kbd>-Taste während des Vergrößerns/Verkleinerns des Video, kann dieses Verhalten deaktiviert werden.

## Zuschneiden von Aufnahmen
![](img/CropVideo.gif)

1. Leinwandgröße auf die gewünschte Auflösung einstellen.
2. Das Video skalieren und zentrieren.
3. Auf den Button `Zuschneiden` (befindet sich oberhalb der Leinwand) klicken.
4. Durch die Verschiebung der Eckpunkte kann das Video zugeschnitten werden.

## PowerPoint Folien exportieren
![](img/SlidesExport.gif)

Um die Folien einer Präsentation als Bild zu exportieren, sind folgende Schritte durchzuführen.

1. Auf Datei klicken.
2. Exportieren.
3. Dateityp ändern.
4. Hier wählt man in der Kategorie Bild-Dateitypen `PNG (Portable Network Graphics) (*.png)` aus.
5. Klicke auf den Button `Speichern unter`.
6. Im Dateidialog Fenster wählt man den Ordner in den die Folien exportiert werden sollen und klickt auf `Speichern`.